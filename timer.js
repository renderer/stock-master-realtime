var soap = require('soap');
var async = require('async');
var LivePrice = require('./models/live-price');
var postal = require('postal');

var vn30 = ["BVH", "CII", "NT2", "HVG", "DPM", "CTG", "KBC", "FPT", "GMD", "HAG", "HPG", "HSG", "EIB", "KDC", "MBB", "MSN", "HHS", "FLC", "HCM", "PPC", "PVD", "PVT", "REE", "ITA", "SSI", "STB", "VCB", "VIC", "VNM", "SBT"];

module.exports = function() {
  var finish = true;

  // TODO: replace with real query method
  function getListTicker(cb) {
    cb(null, vn30);
  }

  function notifyChange(ticker, price) {
    postal.publish({
      channel: "live",
      topic: "price.changed",
      data: {
        ticker: ticker,
        price: price
      }
    });
  }

  function checkForTickerUpdate(client, ticker, cb) {
    client.GetJSQuote({
      symbol: ticker
    }, function(err, result) {
      var quotes = result.GetJSQuoteResult.JSQuote;
      var latestQuote = quotes[quotes.length - 1];
      LivePrice.findOne({
        ticker: ticker
      }, function(err, livePrice) {
        if (!livePrice) {
          LivePrice.create({
            ticker: ticker,
            price: latestQuote.Price
          }, function(err) {
            notifyChange(ticker, latestQuote.Price);
            cb(err);
          });
        } else {
          if (livePrice.price == latestQuote.Price) {
            return cb();
          } else {
            livePrice.price = latestQuote.Price;
            livePrice.save(function(err) {
              notifyChange(ticker, latestQuote.Price);
              cb(err);
            });
          }
        }
      });
    });
  }

  function checkForUpdate(cb) {
    soap.createClient("http://chart.dag.vn/WebServices/QuoteService.asmx?wsdl", function(err, client) {
      if (err) {
        return cb(err);
      }
      getListTicker(function(err, tickers) {
        if (err) {
          return cb(err);
        }
        async.forEach(tickers, function(ticker, cb) {
          checkForTickerUpdate(client, ticker, cb);
        }, function() {
          cb();
        });
      })
    });
  }

  function singleCheckForUpdate() {
    if (!finish) {
      return;
    }
    finish = false;
    checkForUpdate(function() {
      finish = true;
    });
  }

  setInterval(singleCheckForUpdate, 5000);
  singleCheckForUpdate();
};