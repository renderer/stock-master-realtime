var mongoose = require('mongoose');

module.exports = mongoose.model('LivePrice', { ticker: String, price: Number }, 'livePrice');